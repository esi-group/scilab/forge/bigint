# git 
* git add demos/* help/* jar/* macros/* pdf/* tests/*
*  git commit -m " blablba ..."
*  git push origin master

# basic bigint/mbigint
## creation ok
* bigint
* mbigint

## display  ok
* %bigint_p.sci
* %mbigint_p.sci
*  use of string overloading  
    * %bigint_string.sci 
    * %mbigint_string.sci

##check ok
* check_bigint.sci
* check_mbigint.sci

# matrix oprators 

## c: [,]  line concat ok
* %bigint_c_bigint.sci
* %bigint_c_mbigint.sci
* %bigint_c_s.sci
* %mbigint_c_bigint.sci
* %mbigint_c_mbigint.sci
* %mbigint_c_s.sci
* %s_c_bigint.sci
* %s_c_mbigint.sci


## f: [;]  column concat ok
* %bigint_f_bigint.sci
* %bigint_f_mbigint.sci
* %mbigint_f_bigint.sci
* %mbigint_f_mbigint.sci
* %mbigint_f_s.sci
* %bigint_f_s.sci
* %s_f_bigint.sci
* %s_f_mbigint.sci


## i:() insertion  ok
* %bigint_i_mbigint.sci
* %mbigint_i_bigint.sci
* %mbigint_i_mbigint.sci
* %s_i_mbigint.sci
* %mbigint_i_s.sci
* %bigint_i_bigint.sci
* %bigint_i_s.sci
* %s_i_bigint.sci

## e : ()  extraction ok

* %mbigint_e.sci
* %bigint_e.sci  (compatibility `x(1,1)`  ou  `x(1)` pour `x` bigint)

## 0: ' conjuguate ok

* %bigint_0.sci
* %mbigint_0.sci
* 
## t: .' transpose ok

* %bigint_t.sci
* %mbigint_t.sci

##  length/size ok
* size 
* length

# boolean operators  

##  general structure for operator "op"
* %bigint_op_bigint.sci  **main function** 
    *  overloading with double
        * %s_op_bigint.sci
        * %bigint_op_s.sci
    * test bigint_op_bigint.sci
* %mbigint_op_mbigint.sci   **main function** 
    * overloding
        * %bigint_op_mbigint.sci
        * %mbigint_op_bigint.sci
    * overloading with double(matrix)
        * %mbigint_op_s.sci
        * %s_op_mbigint.sci
    * test mbigint_op_mbigint.tst

## 1: < lower ok
* %bigint_1_bigint.sci
* %s_1_bigint.sci
* %bigint_1_s.sci
* %mbigint_1_mbigint.sci
* %bigint_1_mbigint.sci
* %mbigint_1_bigint.sci
* %mbigint_1_s.sci
* %s_1_mbigint.sci

## 2: > greater ok
* %bigint_2_bigint.sci
* %bigint_2_s.sci
* %s_2_bigint.sci
* %mbigint_2_mbigint.sci
* %bigint_2_mbigint.sci
* %mbigint_2_bigint.sci
* %mbigint_2_s.sci
* %s_2_mbigint.sci
* 
## 3: <= lower or equal ok
* %bigint_3_bigint.sci
* %bigint_3_mbigint.sci
* %bigint_3_s.sci
* %mbigint_3_bigint.sci
* %mbigint_3_mbigint.sci
* %mbigint_3_s.sci
* %s_3_bigint.sci
* %s_3_mbigint.sci

## 4: >= greater or equal ok
* %bigint_4_bigint.sci
* %bigint_4_mbigint.sci
* %bigint_4_s.sci
* %mbigint_4_bigint.sci
* %mbigint_4_mbigint.sci
* %mbigint_4_s.sci
* %s_4_bigint.sci
* %s_4_mbigint.sci
##  5: ~ (negation)
*  **not implemented** 
##  n: <> difference ok
* %bigint_n_bigint.sci
* %bigint_n_mbigint.sci
* %bigint_n_s.sci
* %mbigint_n_bigint.sci
* %mbigint_n_mbigint.sci
* %mbigint_n_s.sci
* %s_n_bigint.sci
* %s_n_mbigint.sci
##  o: ==equal ok
* %bigint_o_bigint.sci
* %bigint_o_mbigint.sci
* %bigint_o_s.sci
* %mbigint_o_bigint.sci
* %mbigint_o_mbigint.sci
* %mbigint_o_s.sci
* %s_o_bigint.sci
* %s_o_mbigint.sci
# arithmetic operators

## a: + addition ok
* %bigint_a_bigint.sci
* %bigint_a_mbigint.sci
* %bigint_a_s.sci
* %mbigint_a_bigint.sci
* %mbigint_a_mbigint.sci
* %mbigint_a_s.sci
* %s_a_bigint.sci
* %s_a_mbigint.sci
* implement []+

## s: -  soustraction  ok
* %bigint_s_bigint.sci
* %bigint_s_mbigint.sci
* %bigint_s_s.sci
* %mbigint_s_bigint.sci
* %mbigint_s_mbigint.sci
* %mbigint_s_s.sci
* %s_s_bigint.sci
* %s_s_mbigint.sci
* implement []-

## s : - opposite  ok

* %bigint_s.sci
* %mbigint_s.sci
* **test file** bigint_s.tst

## m: * multiplication ok

* %bigint_m_bigint.sci
* %bigint_m_mbigint.sci
* %bigint_m_s.sci
* %mbigint_m_bigint.sci
* %mbigint_m_mbigint.sci
* %mbigint_m_s.sci
* %s_m_bigint.sci
* %s_m_mbigint.sci
* implement []* 


## x: .* point wize multiplication  ok

* %bigint_x_bigint.sci
* %bigint_x_mbigint.sci
* %bigint_x_s.sci
* %mbigint_x_bigint.sci
* %mbigint_x_mbigint.sci
* %mbigint_x_s.sci
* %s_x_bigint.sci
* %s_x_mbigint.sci
* implement [].*

## p: ^ power ok
* %bigint_p_bigint.sci
* %bigint_p_s.sci
* %s_p_bigint.sci
* %mbigint_p_bigint.sci
* %mbigint_p_s.sci
* %s_p_mbigint.sci
* %bigint_p_mbigint.sci
* ~~%mbigint_p_mbigint.sci~~ **not implemented**

## j: .^ point wize power ok

* %mbigint_j_bigint.sci
* %mbigint_j_mbigint.sci
* %bigint_j_mbigint.sci
* %bigint_j_bigint.sci
* %bigint_j_s.sci
* %mbigint_j_s.sci
* %s_j_bigint.sci
* %s_j_mbigint.sci


##  divisions ok 
* div2.sci
* odd.sci
* divide.sci
* **test files** div2odd.tst and divide.tst
 
## r: / right division 

* %bigint_r_bigint.sci
* %bigint_r_s.sci
* %s_r_bigint.sci
* %mbigint_r_bigint.sci
* %mbigint_r_s.sci
* **TODO   matrix inverse** %mbigint_r_mbigint.sci

##d : ./ point wize right division  ok

* %bigint_d_bigint.sci
* %bigint_d_s.sci
* %mbigint_d_bigint.sci
* %mbigint_d_s.sci
* %s_d_bigint.sci
* %bigint_d_mbigint.sci
* %mbigint_d_mbigint.sci
* %s_d_mbigint.sci


## l: / left division
 
* %bigint_l_bigint.sci
* %bigint_l_s.sci
* %s_l_bigint.sci
* %bigint_l_mbigint.sci
* %s_l_mbigint.sci
* **TODO matrix inverse**

## q : .\ point wize left division ok

* %bigint_q_bigint.sci
* %bigint_q_mbigint.sci
* %bigint_q_s.sci
* %mbigint_q_bigint.sci
* %mbigint_q_mbigint.sci
* %mbigint_q_s.sci
* %s_q_bigint.sci
* %s_q_mbigint.sci

## not yet implemented 
* element wise div/prod
    * *.: u  
    * /. v
    * \. : w
* kronecker
    * .*. : k
    * ./. : y
    * .\. : z

# overloading scilab functions

* abs  ok
* sum ok
* string  "bigint to string"
* sqrt   "square root of bigint"
* **not implemented**
    * prod,min,max,sort  
    * inv

# new functions
* PGCD ok
* powermod  "modular fast exponentiation" ok
* factorize  "successive divisions" ok
* bmodulo   "modular reminder" ok
* millerrabin  "(pseudo)primality test (probalistic)" ok
* brand  "random bigint/mbigint generation" ok
* prime  "(pseudo)prime number generator"







