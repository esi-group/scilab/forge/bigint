//<-- CLI SHELL MODE -->
//=================================

x=123456
y=456789
z=987654
MM=[x y; z x-z]
M=mbigint(MM)
NN=[x -z; -y x-z]
N=mbigint(NN)
P1=M*N
P2=M*NN
P3=MM*N
P4=MM*NN //double verification
 assert_checktrue(and(P1==P4))
 assert_checktrue(and(P2==P4))
 assert_checktrue(and(P3==P4))
 //non-square matrix
MM=[x y; z x-z]
M=mbigint(MM)
NN=[x ; -z]
N=mbigint(NN)
P1=M*N
P2=M*NN
P3=MM*N
P4=MM*NN //double verification
 assert_checktrue(and(P1==P4))
 assert_checktrue(and(P2==P4))
 assert_checktrue(and(P3==P4))


