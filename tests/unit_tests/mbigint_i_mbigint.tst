//<-- CLI SHELL MODE -->
//=================================
// 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
O=bigint(0);
L=[x y z O];
N=[x y z O; y z y x];
P=[x y x O; y z x x];
Q=[L;L]
//  check insertion 
M=[x y z O; O z y x];
M(2,1)=y
assert_checktrue(and(M==N))
M(1:2,3)=x
assert_checktrue(and(M==P))
M=[x y z O; O z y x];
M(2,1:4)=[]
assert_checktrue(and(M==L))
M=[x y z O; O z y x];
M(2,:)=L
assert_checktrue(and(M==Q))
M=[x y z O; O z y x];
M(2,:)=[]
assert_checktrue(and(M==L))
// bigint overloading compatibility
T=zeros(2,4);
T(1,:)=L;
T(2,:)=L;
assert_checktrue(and(T==Q))
T=0;
T(1,:)=L;
T(2,:)=L;
assert_checktrue(and(T==Q))
//scalar overloading compatibility
T=mbigint(zeros(2,4));
T(:,:)=N;
T(2,3:4)=x
T(:,3)=x
assert_checktrue(and(T==P))
T=zeros(2,4)
T(:,:)=N;
T(2,3:4)=x
T(:,3)=x
assert_checktrue(and(T==P))

