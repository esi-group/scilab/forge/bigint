//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
x=bigint('12345678') 
y=bigint('98765432') 
[q,r]=divide(y,x)
rb=bmodulo(y,x)
assert_checktrue(r==rb)
// large bigint
x=bigint('1234567890987654321') 
y=bigint('9876543210123456789') 
[q,r]=divide(y,x)
rb=bmodulo(y,x)
assert_checktrue(r==rb)
// %inf case
x=bigint('1234567890987654321') 
y=%inf
rb=bmodulo(x,y)
assert_checkequal(rb,x)
