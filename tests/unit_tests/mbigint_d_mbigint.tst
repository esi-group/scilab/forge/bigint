//<-- CLI SHELL MODE -->
//=================================

x=123456789
xx=bigint(x)
y=987654321
yy=bigint(y)
M=[y x; -x -y]
MM=mbigint(M)
N=[x x 1 0]
NN=mbigint(N)
P=[x x; 1 0]
PP=mbigint(P)
assert_checkequal(mbigint(floor(M./x)),MM./xx)
assert_checkequal(mbigint(floor(N./x)),NN./xx)
assert_checkequal(mbigint(round(P./M)),PP./MM)
//overloading compatibility
assert_checkequal(mbigint(floor(M./x)),MM./x)
assert_checkequal(mbigint(floor(N./x)),NN./x)
//overloading compatibility
assert_checkequal(mbigint(floor(M./x)),M./xx)
assert_checkequal(mbigint(floor(N./x)),N./xx)
