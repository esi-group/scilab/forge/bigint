//<-- CLI SHELL MODE -->
//=================================

x=123456789
xx=bigint(x)
y=987654321
yy=bigint(y)
M=[y x; -x -y]
MM=mbigint(M)
N=[x x 1 0]
NN=mbigint(N)
P=[x x; 1 0]
PP=mbigint(P)
assert_checkequal(mbigint(floor(x.\M)),xx.\MM)
assert_checkequal(mbigint(floor(x.\N)),xx.\NN)
assert_checkequal(mbigint(round(M.\P)),MM.\PP)
//overloading compatibility
assert_checkequal(mbigint(floor(x.\M)),x.\MM)
assert_checkequal(mbigint(floor(x.\N)),x.\NN)
//overloading compatibility
assert_checkequal(mbigint(floor(x.\M)),xx.\M)
assert_checkequal(mbigint(floor(x.\N)),xx.\N)
