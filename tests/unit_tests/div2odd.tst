//<-- CLI SHELL MODE -->
//=================================
x=bigint('9876543210123') 
y=bigint('1234567890') 
// div2 test
q=div2(y)
assert_checktrue(q==int(1234567890/2))
q=div2(x)
assert_checktrue(q==int(9876543210123/2))
//odd test
r=odd(y)
assert_checktrue(r==0)
r=odd(x)
assert_checktrue(r==1)
