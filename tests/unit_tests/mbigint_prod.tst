//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 

N=10;
L=mbigint(1:N)
s=prod(L)
assert_checkequal(s,bigint(factorial(N)))
M=zeros(2,5)
M(:)=1:N
S1=mbigint(prod(M))
S2=mbigint(prod(M,'c'))
S3=mbigint(prod(M,'r'))
M=mbigint(M)
s1=prod(M)
assert_checkequal(s1,S1)
s2=prod(M,'c') 
assert_checkequal(s2,S2)
s3=prod(M,'r')
assert_checkequal(s3,S3)
//scalar case
NN=bigint(N)
assert_checkequal(NN,prod(NN))

