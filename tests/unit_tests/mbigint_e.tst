//<-- CLI SHELL MODE -->
//=================================
// 
x=bigint(123456789);
y=bigint(987654321);
z=x-y;
O=bigint(0);
L=[x y z O];
M=[L;L];
N=[x;y; z; O];
P=[y z; y z];
Q=[y;y];
assert_checktrue(M(1,4)==O)
assert_checktrue(M(2,1)==x)
assert_checktrue(M(4)==y)
assert_checktrue(L(3)==z)
assert_checktrue(L(1,3)==z)
assert_checktrue(N(2,1)==y)
assert_checktrue(N(2)==y)
assert_checktrue(M(2,:)==L)
assert_checktrue(M(:,2)==Q)
assert_checktrue(M(:,2:3)==P) 
//double overloading
xx=123456789;
yy=987654321;
zz=x-y;
LL=[xx yy zz 0];
PP=[yy zz; yy zz];
QQ=[yy;yy];
assert_checktrue(M(1,4)==0)
assert_checktrue(M(2,1)==xx)
assert_checktrue(M(4)==yy)
assert_checktrue(L(3)==zz)
assert_checktrue(L(1,3)==zz)
assert_checktrue(N(2,1)==yy)
assert_checktrue(N(2)==yy)
assert_checktrue(M(2,:)==LL)
assert_checktrue(M(:,2)==QQ)
assert_checktrue(M(:,2:3)==PP) 
