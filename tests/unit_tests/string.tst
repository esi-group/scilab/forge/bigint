//<-- CLI SHELL MODE -->
//=================================
x='123456789';
xx=bigint(x);
xxx=string(xx);
assert_checkequal(x,xxx)
//negative
x='-123456789';
xx=bigint(x);
xxx=string(xx);
assert_checkequal(x,xxx)
//removing 0 
x='123456789';
xx=bigint('0000'+x);
xxx=string(xx);
assert_checkequal(x,xxx)
//matrix case 
M=[123456789 -123456789; 0 987654321];
MM=mbigint(M);
format('v',20) // long display
assert_checkequal(string(M),string(MM))

