//<-- CLI SHELL MODE -->
//=================================
M=mbigint(grand(2,3,'uin',1,99999999))
M=check_mbigint(M) // OK
//check errors
M.display=string(zeros(2,2))
assert_checkerror("M=check_mbigint(M)",['A.display and A.value has incompatible size !';' [2,2] and [2,3]'])
//
M=mbigint(grand(2,3,'uin',1,99999999))
M.display=zeros(2,3)
assert_checkerror("M=check_mbigint(M)",['A.display is not a matrix of strings!'])
//
M=mbigint(grand(2,3,'uin',1,99999999))
M.value=poly(0,'x')
assert_checkerror("M=check_mbigint(M)",['A.value is not a cell!'])
//
M=mbigint(grand(2,3,'uin',1,99999999))
M.value{2,3}=poly(0,'x')
assert_checkerror("M=check_mbigint(M)",['typeof(A.value{2,3})=polynomial'])


