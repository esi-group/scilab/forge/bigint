//<-- CLI SHELL MODE -->
//=================================
// tests with integers <10^16 
//vector
p=1,n=5
L=mbigint(grand(p,n,'uin',1,99999999))
k=length(L)
assert_checkequal(k,p*n)
//vector
p=4,n=1
L=mbigint(grand(p,n,'uin',1,99999999))
k=length(L)
assert_checkequal(k,p*n)
//matrix
p=4,n=3
L=mbigint(grand(p,n,'uin',1,99999999))
k=length(L)
assert_checkequal(k,p*n)
//scalar
p=1,n=1
L=mbigint(grand(p,n,'uin',1,99999999))
k=length(L)
assert_checkequal(k,p*n)
