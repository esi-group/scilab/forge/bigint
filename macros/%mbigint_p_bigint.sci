function z=%mbigint_p_bigint(x,y)
    // binary operator &#94; (power) for bigint
    // Calling Sequence
    //   z=x^y
    //
    // Parameters
    //  x,z : mbigint
    //    y : bigint
    //
    // Description
    // bigint power using fast exponentiation 
    // <latex> x^{2k+1}= x\times (x^2)^k</latex>  or   <latex> x^{2k}= (x^2)^k</latex> 
    //
    // Examples
    // 
    // format('v',20)
    // M=mbigint([1 2 ; 3 4])
    // e=bigint(10)
    // M^e
    // [1 2 ; 3 4]^10
    // 
    // See also
    //  mbigint
    // odd
    // div2
    // bigint_m_bigint
    //
    // Authors
    //  Philippe Roux
    //

     z=1+0*x  //  x^0= 1
    if y<0 then // not integer !
        error('not implemented')
    else //x,y>0  
        z=1
        while y>0
            if odd(y)==1 then 
                z=x*z
            end
            x=x*x
            y=div2(y)
        end
    end
    z=check_mbigint(z)
endfunction
