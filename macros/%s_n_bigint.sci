function [bool]=%s_n_bigint(x,y)
    // overloading  compatibility for binary operator " &#60;&#62;" (not equal)with double/bigint
    // Calling Sequence
    //   x<>y
    //   bool=(x<>y)
    //
    // Parameters
    // y : bigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // scalar/bigint difference
    //
    // Examples
    //
    // x=123456789
    // y=bigint('123456789') 
    // x<>y  //  false
    // x=12345678
    // x<>y // true
    // [123456789 12345678]<>y // matrix case
    // 
    // See also
    //  mbigint
    // mbigint_n_bigint
    // bigint_n_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    bool=(x<>y)
endfunction
