function [bool]=%bigint_2_s(x,y)
    // overloading  compatibility for &#62; (greater) operator with bigint/double/mbigint
    // Calling Sequence
    //       x>y
    //   bool=(x>y)
    //
    // Parameters
    // x : bigint
    // y : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint greater  operator
    //
    // Examples
    //
    // x=bigint('123456789')
    // y=1234567890
    // x>y  //  false
    // y=12345678
    // x>y // true
    // 
    // See also
    //  bigint
    // bigint_2_bigint
    // bigint_2_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert y to bigint
    y=mbigint(y)
    bool=(x>y)
endfunction
