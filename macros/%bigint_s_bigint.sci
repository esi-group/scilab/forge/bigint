function [z]=%bigint_s_bigint(x,y)
    // binary operators - (minus) for bigint
    // Calling Sequence
    //   z=x-y
    //
    // Parameters
    //  x,y,z: bigint
    //
    // Description
    // bigint soustraction 
    //
    // Examples
    // 
    // y=bigint('123456789') 
    // x=bigint('987654321') 
    // x-y  //  bigint addition
    // 987654321-123456789 // double addition
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //

    if (x.signe==-1)&(y.signe==-1) then   z=-((-x)-(-y))
    elseif (x.signe==1)&(y.signe==-1) then z=x+(-y)
    elseif (x.signe==-1)&(y.signe==1) then z=-((-x)+y)
    elseif x<y then z=-(y-x)
    else z=bigint('0')
        //enlarge x,y,z representation 
        nx=length(x.rep)
        ny=length(y.rep)
        n=max(nx,ny)
        x.rep=[x.rep;zeros(n-nx,1)]
        y.rep=[y.rep;zeros(n-ny,1)]
        z.rep=zeros(n,1)
        retenue=0
        for k=1:n
            z.rep(k)=x.rep(k)-y.rep(k)-retenue
            if z.rep(k)<0 then 
                z.rep(k)=10^7+z.rep(k)
                retenue =1 
            else retenue=0
            end
        end
    end
    z=check_bigint(z)
endfunction
