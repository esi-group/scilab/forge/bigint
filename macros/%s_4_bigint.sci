function [bool]=%s_4_bigint(x,y)
    // overloading  compatibility for &#8805; (greater or equal) operator with double/bigint
    // Calling Sequence
    //   x>=y
    //   bool=(x>=y)
    //
    // Parameters
    // y : bigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint "greater or equal" operator
    //
    // Examples
    //
    // x=12345678
    // y=bigint('123456789')
    // x>=y  //  false
    // x=1234567890
    // x>=y // true
    // (x*ones(2,2))>=y  // matrix case
    // 
    // See also
    //  mbigint
    // bigint_4_bigint
    // mbigint_4_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    bool=(x>=y)
endfunction
