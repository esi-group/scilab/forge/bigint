function [bool]=%mbigint_o_bigint(A,x)
    // overloading compatibility for == (equal) operator with mbigint/bigint
    // Calling Sequence
    //      A==x
    //   bool=(A==x)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // bigint/mbigint "equal" comparison
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // M==x
    // M==y
    // M==z
    // 
    // See also
    //  mbigint
    // bigint_o_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    bool=(zeros(p,n)==0)
    for i=1:p
        for j=1:n
            bool(i,j)=(A.value{i,j}==x)
        end
    end
endfunction
