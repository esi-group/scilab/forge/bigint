function [C]=%bigint_j_mbigint(A,B)
    //  binary operators .&#94; (element wise power) with bigint/mbigint
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  B,C: mbigint
    //    A : bigint
    //
    // Description
    //bigint/ mbigint "element wise power" operator
    //
    // Examples
    // //
    // x=bigint(12345)
    // M=mbigint([0 1; 2 3])
    // x.^N
    // x^2
    // x^3
    // 
    // See also
    //  mbigint
    // bigint_p_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(B)
    C=B
    for i=1:p
        for j=1:n
            b=B.value{i,j}
            c=(A^b)
            C.value{i,j}=c
            C.display(i,j)=string(c)
            //typeof(A.value(i,j))= ce(ll) 
            //typeof{A.value(i,j})= bigint
        end
    end
endfunction
