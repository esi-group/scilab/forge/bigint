function M=%mbigint_i_s(varargin)
    // overloading compatibility for insertion operator () with bigint/double 
    // Calling Sequence
    //   M(i,j)=x
    //
    // Parameters
    // M : double
    // i,j : integers
    // x : mbigint
    //
    // Description
    // insert element x in the i-th line j-th column of bigint matrix M. you can use  vector notations for indexes as  M(a:b,c) M(a,b:c), M(a:b,c:d), M(a,:) M(:,b) M([...]) ....
    //
    // Examples
    // 
    // x=bigint(2)^57-1 
    // x(2:3,4:5)=2
    // 
    // See also
    //  matrix_bigint
    // mbigint
    // mbigint_i_bigint
    // mbigint_i_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    M=mbigint(varargin($))
    M(varargin(1:$-2))=varargin($-1)
endfunction
