function [C]=%mbigint_j_s(A,B)
    // overloading compatibility for binary operators .&#94; (power) with bigint/mbigint
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  A,C: mbigint
    //    B : double
    //
    // Description
    // mbigint "element wise power" operator
    //
    // Examples
    // //
    // x=bigint('12345') 
    // y=bigint('98765') 
    // M=[x y; y x]
    // N=3
    // M.^N
    // y^3
    // x^3
    // 
    // See also
    //  mbigint
    // mbigint_j_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    B=bigint(B)
    C=A.^B
endfunction
