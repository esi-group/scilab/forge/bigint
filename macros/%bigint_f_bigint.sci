function M=%bigint_f_bigint(x,y)
    // overloading compatibility for column concatenation [;] for  bigint
    // Calling Sequence
    //   M=[x;y]
    //
    // Parameters
    //  M : mbigint
    // x,y : bigint
    //
    // Description
    // concate two bigint to create a column mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // M=[x;y]
    // typeof(M)
    // 
    // See also
    //  matrix_bigint
    // mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    T=cell(2,1)
    T{1,1}=x
    T{2,1}=y
    M=mlist(['mbigint','display','value'],..
      [string(x);string(y)],T)  
endfunction
