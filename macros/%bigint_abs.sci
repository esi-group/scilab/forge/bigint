function [x]=%bigint_abs(x)
    // overloading absolute value (abs) for bigint
    // Calling Sequence
    //   x=abs(x)
    //
    // Parameters
    //  x: bigint
    //
    // Description
    // bigint absolute value
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // sign(x)
    // y=bigint('-98765432') 
    // sign(y)
    // sign(bigint(0))
    // 
    // See also
    //  abs
    //  bigint
    //  bigint_sign
    //
    // Authors
    //  Philippe Roux
    //

    x.signe=1
    x=check_bigint(x)
endfunction
