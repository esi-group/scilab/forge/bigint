function M=%s_c_mbigint(x,L)
    // overloading compatibility for line concatenation for  double/mbigint 
    // Calling Sequence
    //   M=[x L]
    //
    // Parameters
    // M,L: mbigint
    // x : double
    //
    // Description
    // concatenate a double and a mbigint to a line mbigint 
    //
    // Examples

    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x y]
    // M=[1 L]            // scalar case
    // L=[x y ; y x]
    // M= [zeros(2,2) L]  // matrix case
    // M=[ [], L]        // empty case
    // 
    // See also
    //  mbigint
    // mbigint_c_mbigint
    // bigint_c_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    if x==[] then M=L
    else x=mbigint(x)
        M=[x,L]  
    end  
endfunction
