function [B]=%s_q_mbigint(x,A)
    // overloading compatibility for binary operators .\ (element wise left divide) with double/mbigint
    // Calling Sequence
    //   B=x.\A
    //
    // Parameters
    //  A,B: mbigint
    //    x: double
    //
    // Description
    // double/mbigint "element wise divide" operator
    //
    // Examples
    // //
    // x=123456789
    // xx=bigint(x) 
    // y=9876543
    // yy=bigint(y) 
    // x*y
    // M=[xx yy; -xx -xx]
    // N=[y x; y -y]
    // N.\M
    // 
    // See also
    //  mbigint
    // mbigint_q_mbigint
    // bigint_q_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    B=x.\A
    endfunction
