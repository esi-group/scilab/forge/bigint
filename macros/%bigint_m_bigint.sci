function [z]=%bigint_m_bigint(x,y)
    // binary operator * (product) for bigint
    // Calling Sequence
    //   z=x*y
    //
    // Parameters
    //  x,y,z: bigint
    //
    // Description
    // bigint product using Karatsuba algorithm with "digits" of size 10^7
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // y=bigint('98765432') 
    // x*y  //  bigint product
    // 12345678*98765432 // double product
    // x=bigint('123456789987654321')
    // y=bigint('987654321123456789')
    // z=x*y
    // string(z)=='121932632103337905662094193112635269'
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //

    [lhs,rhs]=argn(0);
    if (x.signe==-1)&(y.signe==-1) then //x*y=(-x)*(-y)
        z=(-x)*(-y)
    elseif (x.signe==1)&(y.signe==-1) then //x*y=-(x*(-y))
        z=-(x*(-y))
    elseif (x.signe==-1)&(y.signe==1) then //x*y=-(y*(-x))
        z=-((-x)*y)
    else //enlarge x,y represantation 
        nx=length(x.rep)
        ny=length(y.rep)
        n=max(nx,ny)
        x.rep=[x.rep;zeros(n-nx,1)]
        y.rep=[y.rep;zeros(n-ny,1)]
        //a=a0+a1(10^7)^N
        if n==1 then
            z=bigint(msprintf('%16.0f',x.rep*y.rep))
        else N=ceil(n/2)
            z=bigint('0')
            x0=z,x1=z,y0=z,y1=z
            x0.rep=x.rep(1:N)
            x1.rep=x.rep(N+1:$)
            y0.rep=y.rep(1:N)
            y1.rep=y.rep(N+1:$)
            // karatsuba algorithm
            p1=x0*y0
            p3=x1*y1
            p2=p1+p3-((x1-x0)*(y1-y0))
            p3.rep=[zeros(2*N,1);p3.rep]// * (10^7)^(2*N)
            p2.rep=[zeros(N,1);p2.rep] // * (10^7)^N            
            z=(p1+p2)+p3
        end 
    end
    z=check_bigint(z)
endfunction
