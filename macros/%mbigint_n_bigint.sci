function [bool]=%mbigint_n_bigint(A,x)
    // overloading compatibility for " &#60;&#62;" (not equal) operator mbigint/bigint
    // Calling Sequence
    //   A<>x
    //
    // Parameters
    //  A: mbigint
    //  x: bigint
    //  bool : boolean
    //
    // Description
    // mbigint/bigint "not equal" comparison
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // A=[x y]
    // B=[y x]
    // C=[x x]
    // A<>x  
    // B<>x  
    // C<>x
    // 
    // See also
    //  mbigint
    // mbigint_o_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    bool=~(A==x)
endfunction
