function [C]=%mbigint_j_mbigint(A,B)
    // binary operators .&#94; (element wise power) for mbigint
    // Calling Sequence
    //   C=A.^B
    //
    // Parameters
    //  A,B,C: mbigint
    //
    // Description
    // mbigint "element wise power" operator
    //
    // Examples
    // //
    // x=bigint('12345') 
    // y=bigint('98765') 
    // M=[x y; y x]
    // N=mbigint([0 1; 2 3])
    // M.^N
    // y^2
    // x^3
    // 
    // See also
    //  mbigint
    // bigint_p_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
    [p1,n1]=size(B)
    if (p<>p1)|(n<>n1) then
        error('incompatible size!')
    else
        C=mbigint(zeros(p,n))
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=B.value{i,j}
                c=(a^b)
                C(i,j)=c
            end
        end
    end
endfunction
