function [C]=%s_p_mbigint(A,B)
    // overloading compatibility for operators &#94; (power) with double/mbigint
    // Calling Sequence
    //   C=A^B
    //
    // Parameters
    //  B,C: mbigint
    //    A : double (scalar not a matrix!)
    //
    // Description
    // double/mbigint "power" operator
    //
    // Examples
    // //
    // x=12345
    // M=mbigint([0 1; 2 3])
    // x^M
    // 
    // See also
    //  mbigint
    // bigint_p_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    A=bigint(A)
    A=check_bigint(A)//  A can't be a matrix 
    C=A.^B
endfunction
