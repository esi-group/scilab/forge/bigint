function [bool]=%bigint_o_s(x,y)
    // overloading  compatibility for == (equal) with bigint/double
    // Calling Sequence
    //       x==y
    //   bool=(x==y)
    //
    // Parameters
    // x : bigint
    // y : double(scalar)
    // bool : boolean
    //
    // Description
    // bigint/double "equal"  operator
    //
    // Examples
    //
    // x=bigint('123456789')
    // y=123456789
    // x==y  //  True
    // y=12345678
    // x==y // false
    // 
    // See also
    //  mbigint
    // bigint_o_bigint
    // bigint_o_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    y=mbigint(y)
    bool=(x==y)
endfunction
