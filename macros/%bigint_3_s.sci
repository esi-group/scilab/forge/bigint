function [bool]=%bigint_3_s(x,y)
    // overloading  compatibility for &#8804; (lower or equal) with bigint/double
    // Calling Sequence
    //       x<=y
    //   bool=(x<=y)
    //
    // Parameters
    // x : bigint
    // y : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint "lower or equal" operator
    //
    // Examples
    //
    // x=bigint('123456789')
    // y=1234567890
    // x<=y  //  True
    // y=12345678
    // x<=y // false
    // 
    // See also
    //  bigint
    // mbigint
    // bigint_3_bigint
    // bigint_3_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    y=mbigint(y)
    bool=(x<=y)
endfunction
