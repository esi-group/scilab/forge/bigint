function M=mbigint(varargin)
    // matrix_bigint
    // Calling Sequence
    // [M]=mbigint(p,n,m_11,...m_pn)
    // [M]=mbigint(A)
    //
    // Parameters
    // p,n: integers
    // M: mbigint
    //  m_ij : bigint entries of M
    //   A : matrix of double
    //
    // Description
    // create a "matrix of big integers" of size pxn with coefficient <latex> m_{i,j}</latex>varagin (set by columns). 
    //  
    //
    // Examples
    // //convert double matrix to mbigint
    // M=mbigint(grand(2,3,'uin',1,99999999))
    // typeof(M)  //  mbigint
    // x=mbigint(123456789)
    // typeof(x)  // simplification to bigint
    // // create a matrix of bigint
    // x=bigint(123456789)
    // y=bigint(987654321)
    // z=x-y
    // M=mbigint(2,2,x,y,z,x-z)  
    //
    // See also
    // bigint
    //
    // Authors
    // Philippe Roux
    //

    [lhs ,rhs]=argn()
    if rhs==1 then //convert double matrix to bigint matrix
        A=varargin(1)
        [p,n]=size(A)
        if p*n==1 then 
            M=bigint(A)
        else
            T=cell(p,n)
            D=string(zeros(p,n))
            for j=1:n
                for i=1:p
                    T{i,j}=bigint(A(i,j))
                    D(i,j)=stripblanks(msprintf('%16.0f',A(i,j)))
                end
            end
            M=mlist(['mbigint','display','value'],D,T)
        end
    else // create a mbigint
        p=varargin(1)
        n=varargin(2)
        if rhs-2<p*n then
            error("number of  coefficients M(i,j)< "+string(p*n))
        end
        k=3
        T=cell(p,n)
        D=string(zeros(p,n))
        for j=1:n
            for i=1:p
                m=bigint(varargin(k))
                T{i,j}=m
                D(i,j)=string(m)
                k=k+1
            end
        end
        M=mlist(['mbigint','display','value'],D,T)
    end
endfunction
