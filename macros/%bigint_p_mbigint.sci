function [C]=%bigint_p_mbigint(A,B)
    // overloading compatibility for binary operators .&#94; (power) with bigint/mbigint
    // Calling Sequence
    //   C=A^B
    //
    // Parameters
    //  B,C: mbigint
    //    A : bigint
    //
    // Description
    // bigint/mbigint power operator, in this cas power is computed "element wise"
    //
    // Examples
    // //
    // x=bigint(12345)
    // M=mbigint([0 1; 2 3])
    // x^N
    // x^2
    // x^3
    // 
    // See also
    //  mbigint
    // bigint_j_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    C=A.^B
endfunction
