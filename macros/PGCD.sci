function [d]=PGCD(x,y)
    // bigint  Greatest Comon Divisor
    // Calling Sequence
    //   [d]=PGCD(x,y)
    //
    // Parameters
    //  x,y,d: bigint
    //
    // Description
    // bigint Greatest Comon Divisor using Euclide algorithm 
    //  <latex>\forall  k\in {\mathbb Z},\,\, k|x\,et\, k|y\Rightarrow d|k</latex>
    //
    // Examples
    // 
    // x=bigint('98765432') 
    // y=bigint('12345678') 
    // d=PGCD(x,y) 
    // a=x/d
    // b=y/d
    // PGCD(a,b) // =1
    // x=bigint('123456789987654321')
    // y=bigint('987654321123456789')
    // PGCD(x,y)//= 1222222221 
    // 
    // See also
    //  bigint
    // divide
    // abs
    //
    // Authors
    //  Philippe Roux
    //

    
    if and(x<1e9)&and(y<1e9) then //  for "small" integers use pmodulo (increase speed)
        x=iconvert(x,0)// iconvert -> double ?scilab6 bug?
        y=iconvert(y,0)
        d=iconvert(gcd([x y]),0)//  gcd can't compute above 1e9
    else //convert to bigint if necessary (=> rounding errors)
        if type(x)==1 then x=bigint(x)
        end
        if type(y)==1 then y=bigint(y)
        end
        // check division by 0
        if y==0 then error('division by 0 !')
        end
        // reduce to x,y>0 
        x=abs(x)
        y=abs(y)
        while y<>0
            [q,r]=divide(x,y)
            x=y
            y=r
        end
        d=x
    end
endfunction
