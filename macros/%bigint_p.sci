function %bigint_p(x)
    // bigint display
    // Calling Sequence
    //   %bigint_p(x)
    //   x
    //
    // Parameters
    //  x: bigint
    //
    // Description
    // display bigint as the string of its digits
    //
    // Examples
    // x=bigint('-1234567890') 
    // x
    // 
    // See also
    //  bigint
    // string
    //
    // Authors
    //  Philippe Roux
    //
    
    str=string(x)
    disp(str)
endfunction
