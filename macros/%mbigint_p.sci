function %mbigint_p(M)
    // display bigint matrix
    // Calling Sequence
    //   M
    //
    // Parameters
    //  M : mbigint
    //
    // Description
    // a matrix of bigint is a mlist of type 'mbigint' with two fields
    //<itemizedlist>
    // <listitem> 
    // <para>value : the cell containing the matrix of bigint </para>
    // </listitem> 
    // <listitem> 
    // <para>display : a string matrix (same size as 'value') with string representation of bigint to display </para>
    // </listitem> 
    // </itemizedlist> 
    //
    // Examples
    // 
    //x=bigint(123456789);
    //y=bigint(987654321);
    //z=x-y;
    //T=cell();
    //T{1,1}=x;
    //T{1,2}=y;
    //T{2,1}=z;
    //T{2,2}=bigint(0);
    //M=mlist(['mbigint','display','value'],[string(x) string(y); string(z) '0' ],T)
    //typeof(M)
    // 
    // See also
    //  mbigint
    // bigint_p
    // string
    //
    // Authors
    //  Philippe Roux
    //
    
  disp(M.display)
endfunction
