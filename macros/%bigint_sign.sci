function [s]=%bigint_sign(x)
    // overloading sign  for bigint
    // Calling Sequence
    //   s=sign(x)
    //
    // Parameters
    //  x: bigint
    //  s: +1,-1 or 0
    //
    // Description
    // bigint sign :  if x=0  then s=0 , else s=x.signe
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // abs(x)
    // y=bigint('-98765432') 
    // abs(y)
    // 
    // See also
    // sign
    //  bigint
    //  bigint_abs
    //
    // Authors
    //  Philippe Roux
    //

    if x==0 then s=0
    else s=x.signe
    end
endfunction
