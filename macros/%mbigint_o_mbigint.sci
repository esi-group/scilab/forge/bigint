function [bool]=%mbigint_o_mbigint(A,B)
    // binary "==" equal operator for mbigint
    // Calling Sequence
    //   A==B
    //
    // Parameters
    //  A,B: mbigint
    //  bool : boolean
    //
    // Description
    // mbigint equal comparison
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('0123456789') 
    // A=[x y]
    // B=[y x]
    // C=[x x]
    // A==A  // true 
    // A==B  // false
    // A==C
    // 
    // See also
    //  mbigint
    // bigint_o_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [p,n]=size(A)
    [p1,n1]=size(B)
    if (p<>p1)|(n<>n1) then
        error('matrix with different size!')
    else
        bool=(zeros(p,n)==0)
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                b=B.value{i,j}
                bool(i,j)=(a==b)
                //typeof(A.value(i,j))= ce(ll) 
                //typeof{A.value(i,j})= bigint
            end
        end
    end
endfunction
