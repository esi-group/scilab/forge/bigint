function [M]=%bigint_sum(M,varargin)
    //  overloading compatibility for bigint matrix sum
    // Calling Sequence
    //   M=sum(M [,opt])
    //
    // Parameters
    //  M : bigint
    // opt : string 'c' or 'r'
    //
    // Description
    // sum of the bigint matrix entries (M(1,1) here) you can specify sum over rows/column with optional argument 'r' ou 'c' .
    //
    // Examples
    // 
    // M=bigint(123456789);
    // sum(M)
    // sum(M,'r') 
    // sum(M,'c') 
    // 
    // See also
    //  size
    // bigint
    // mbigint_sum
    // sum
    //
    // Authors
    //  Philippe Roux
    //
    
     //nothing to do !!!
endfunction
