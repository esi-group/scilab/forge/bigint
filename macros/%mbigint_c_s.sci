function M=%mbigint_c_s(L,x)
    // overloading compatibility  for line concatenation with  mbigint/double
    // Calling Sequence
    //   M=[L x]
    //
    // Parameters
    // M,L: mbigint
    // x : double
    //
    // Description
    // concatenate a mbigint with a double, on its right, to a line mbigint 
    //
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // L=[x y]
    // M=[L 1]            // scalar case
    // L=[x y ; y x]
    // M= [L zeros(2,2)]  // matrix case
    // M=[ L , [] ]        // empty case
    // 
    // See also
    //  mbigint
    // mbigint_c_mbigint
    // mbigint_c_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    if x==[] then M=L
    else x=mbigint(x)
        M=[L x]  
    end
endfunction
