function M=%bigint_f_s(x,y)
    // overloading compatibility for column concatenation [;] with  bigint/double
    // Calling Sequence
    //   M=[x;y]
    //
    // Parameters
    // x : bigint
    // y : double
    //
    // Description
    // concatenate  a bigint and a double to a column mbigint 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=1
    // L=[x;y]
    // y=[1:10]'   // matrix case
    // L=[x;y]
    // y=[]        // empty case
    // L=[x;y]
    // 
    // See also
    //   mbigint
    //
    // Authors
    //  Philippe Roux
    //


    [p,n]=size(y)
    if y==[] then
        T=cell(1,1)
        T{1,1}=x
        M=mlist(['mbigint','display','value'],..
        string(x),T)
    elseif p*n==1 then 
        y=bigint(y)
        T=cell(2,1)
        T{1,1}=x
        T{2,1}=y
        M=mlist(['mbigint','display','value'],..
        [string(x);string(y) ],T)  
        else y=mbigint(y)
        T=cell(n+1,1)
        T{1,1}=x
        T(2:n+1,1)=y.value
        M=mlist(['mbigint','display','value'],..
        [ string(x);y.display],T) 
    end
endfunction
