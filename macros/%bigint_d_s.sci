function [q]=%bigint_d_s(x,y)
    // overloading  compatibility for ./ (element wise divide) operator  for bigint/double
    // Calling Sequence
    //   q=x./y
    //
    // Parameters
    // x,q: bigint
    // y : scalar (double)
    //
    // Description
    // bigint/double  "element wise divide" operator
    //
    // Examples
    // 
    // x=bigint('98765432')
    // y=12345678
    // q=x./y  //  bigint division
    // 98765432/12345678
    // 
    // See also
    //  bigint
    // bigint_d_bigint
    // bigint_d_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    y=mbigint(y)
    q=x./y
endfunction
