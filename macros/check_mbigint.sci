function A=check_mbigint(A)
    // bigint matrix
    // Calling Sequence
    // [A]=check_mbigint(A)
    //
    // Parameters
    // A: mbigint
    //
    // Description
    // check mbigint structure, if not returns error. Always ends yours macros returning a "mbigint" with the command z=check_bigint(z) this will return a normalized form for z.
    //
    // Examples
    //
    // x=bigint('1234567890')
    // y=bigint('9876543210')
    // z=x-y
    // T=cell(2,2)
    // T{1,1}=x;T{1,2}=y;
    //T{2,1}=z;T{2,2}=bigint(0)
    // D=[string(x) string(y);string(z),'0']
    // M=mlist(['mbigint','display','value'],D,T)
    // M=check_mbigint(M) // ok
    // M=mlist(['mbigint','display','value'],'0',T)
    // M=check_mbigint(M) // error
    // M=mlist(['mbigint','display','value'],D,x)
    // M=check_mbigint(M) // error
    //
    // See also
    // mbigint
    // check_bigint
    //
    // Authors
    // Philippe Roux
    //

    [lhs,rhs] = argn(0);
    // check type
    // check fields
    if ~((typeof(A)=='mbigint' )&and(fieldnames(A)== ['display';'value'])) then
        error('not mbigint structure')
    end
    // A.display
    if type(A.display)<>10 then
        error('A.display is not a matrix of strings!')
    end
    // A.value
    if type(A.value)<>17 then
        error('A.value is not a cell!')
    end
     //check matrix sizes
    [p,n]=size(A.display)
    [p1,n1]=size(A.value)
    if (p<>p1)|(n<>n1) then
        error(msprintf('A.display and A.value has incompatible size !\n [%d,%d] and [%d,%d]',p,n,p1,n1))
    end
    // A.value{i,j} type
    for i=1:p
        for j=1:n
            str=typeof(A.value{i,j})
            if str<>"bigint" then 
                error(msprintf("typeof(A.value{%d,%d})=%s\n",i,j,str))
            end
        end
    end
endfunction
