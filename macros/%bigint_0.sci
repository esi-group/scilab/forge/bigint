function x=%bigint_0(x)
    // overloading compatibility for transpose operator with bigint
    // Calling Sequence
    //   x.'
    //
    // Parameters
    //  x : mbigint
    //
    // Description
    // transposition T(i,j)=M(j,i)
    //
    // Examples
    // 
    // x=bigint('123456789') 
    // x.'  // =x
    // 
    // See also
    // mbigint_0
    // bigint_t
    //
    // Authors
    //  Philippe Roux
    //
    
   // nothing to do
endfunction
