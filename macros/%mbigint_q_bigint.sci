function [B]=%mbigint_q_bigint(A,x)
    // overloading compatibility for binary operators .\ (element wise left divide) with bigint/mbigint
    // Calling Sequence
    //   B=A.\x
    //
    // Parameters
    //  A,B: mbigint
    //    x: bigint
    //
    // Description
    // mbigint "element wise left divide" operator
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // M=[y x; -x -y]
    // N=[x x;x x]
    // M.\x
    // M.\x
    // 
    // See also
    //  mbigint
    // bigint_r_bigint
    //
    // Authors
    //  Philippe Roux
    //

    [p,n]=size(A)
        B=A
        for i=1:p
            for j=1:n
                a=A.value{i,j} 
                B(i,j)=(x/a)
            end
        end
    endfunction
