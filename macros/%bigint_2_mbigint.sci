function [bool]=%bigint_2_mbigint(x,A)
    // overloading compatibility for &#62; (greater) operator with bigint/mbigint
    // Calling Sequence
    //   bool=(x>A)
    // Parameters
    //  A: mbigint
    //  x : bigint
    //  bool : boolean matrix
    //
    // Description
    // bigint/mbigint  greater  operator
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // x>M
    // y>M
    // z>M
    // 
    // See also
    //  mbigint
    // mbigint_1_bigint
    //
    // Authors
    //  Philippe Roux
    //

    bool=(A<x)
endfunction
