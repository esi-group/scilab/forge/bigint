function [z]=%bigint_a_bigint(x,y)
    // binary operator + for bigint
    // Calling Sequence
    //   z=x+y
    //
    // Parameters
    //  x,y,z: bigint
    //
    // Description
    // bigint  addition (basic algorithm with carrying) 
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('987654321') 
    // x+y  //  bigint addition
    // 123456789+987654321 // double addition
    // 
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    [lhs,rhs]=argn(0);
    if (x.signe==-1)&(y.signe==-1) then //x+y=-((-x)+(-y))
        z=-((-x)+(-y))
        elseif (x.signe==1)&(y.signe==-1) then //x+y=(x-(-y))
        z=(x-(-y))
        elseif (x.signe==-1)&(y.signe==1) then //x+y=(y-(-x))
        z=(y-(-x))
    else z=bigint('0')
        //enlarge x,y,z represantation 
        nx=length(x.rep)
        ny=length(y.rep)
        n=max(nx,ny)
        x.rep=[x.rep;zeros(n-nx,1)]
        y.rep=[y.rep;zeros(n-ny,1)]
        // adding digits (in base 10^7 )
        retenue=0
        for k=1:n
            z.rep(k)=x.rep(k)+y.rep(k)+retenue
            reste=pmodulo(z.rep(k),1e+7)
            retenue=(z.rep(k)-reste)/(1e+7)
            z.rep(k)=reste
        end
        z.rep($+1)=retenue
    end
    z=check_bigint(z)
endfunction
