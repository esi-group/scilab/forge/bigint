function [q]=%bigint_r_s(x,y)
    // overloading  compatibility for / (right divide) operator with bigint/double
    // Calling Sequence
    //   q=x/y
    //
    // Parameters
    // x,q: bigint
    // y : scalar (double) 
    //
    // Description
    // bigint/double "right division"
    //
    // Examples
    // 
    // x=bigint('98765432')
    // y=12345678
    // q=x/y  //  bigint division
    // 98765432/12345678
    // 
    // See also
    //  mbigint
    // bigint_r_bigint
    // bigint_r_mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
    y=mbigint(y)
    q=x/y
endfunction
