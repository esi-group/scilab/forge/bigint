function q=%bigint_q_bigint(x,y)
    // binary operator .\ (element wise left divide) for bigint
    // Calling Sequence
    //   q=x.\y
    //
    // Parameters
    //  x,y,q: bigint
    //
    // Description
    // bigint "element wise left divide" reduce to macro divide .
    //
    // Examples
    // 
    // y=bigint('98765432') 
    // x=bigint('12345678') 
    // q=x\y  //  bigint division
    // 98765432/12345678
    // r=x-q*y 
    // pmodulo(98765432,12345678) //  division with doubles
    // x=bigint('1234567890')
    // y=bigint('987654321123456789')
    // q=y.\x
    // r=x-q*y
    // x== q*y+r
    // r<abs(x)
    // 
    // See also
    //  bigint
    // divide
    //
    // Authors
    //  Philippe Roux
    //

    [q,r]=divide(y,x)
endfunction

