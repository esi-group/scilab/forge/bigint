function [r]=bmodulo(a,b)
    // Euclidian reminder "pmodulo" for bigint
    // Calling Sequence
    //   [c]=bmodulo(a,b)
    //
    // Parameters
    //  a,b,r: bigint
    //
    // Description
    //  positive reminder of Euclidian division a/b <=> a=c+b*k   (k integer and 0<=c<b), if a is a matrix  then the result is a matrix with same size and r(i,j)=bmodulo(a(i,j),b)
    //
    // Examples
    // //
    // x=bigint('1234567898765543210') 
    // y=bigint('9876543210123456789') 
    // [q,r]=divide(y,x)
    // bmodulo(y,x)// = r
    // bmodulo([x,y],x)//= [0,r]
    // 
    // See also
    //  divide
    // pmodulo
    //
    // Authors
    //  Philippe Roux
    //

    //  for "small" integers use pmodulo (increase speed)
    [p,n]=size(a)
    if p*n>1 then //matrix case
        r=zeros(p,n)
        for i=1:p
            for j=1:n
                r(i,j)=bmodulo(a(i,j),b)
            end
        end
    elseif and(a<1e15)&and(b<1e15) then 
if type(a)<>1 then  a=iconvert(a,0)
end
if type(b)<>1 then  b=iconvert(b,0)
end
        r=pmodulo(a,b)
    else [q,r]=divide(a,b)  // "division by 0" already checked in divide
    end
endfunction
