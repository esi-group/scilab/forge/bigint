function [varargout]=%bigint_size(varargin)
    // overloading  compatibility for size for bigint
    // Calling Sequence
    //   [p[,n]]=size(x[,opt])
    //
    // Parameters
    //  x: bigint
    //  opt : double or integer
    //  p,n : integers
    //
    // Description
    // size of bigint is 1x1 ( bigint considered as a mbigint with 1 cell)
    //
    // Examples
    // //
    // x=bigint('12345678') 
    // [p,n]=size(x) // = p=1,n=1
    // p=size(x,1) // = p=1
    // n=size(x,2) // = n=1
    // 
    // See also
    //  size
    // length
    //
    // Authors
    //  Philippe Roux
    //

    [lhs,rhs]=argn()
    if (rhs>1) then opt=varargin(2)
        else opt=0
    end
    p=1,n=1
    if opt==0 then
        if lhs==2 then
            varargout=list(p,n)
        elseif lhs==1 then 
            varargout=list([p,n])
            else error('not implemented')
        end
        elseif (opt==1)|(opt=='r') then 
        varargout=list(p)
        elseif (opt==2)|(opt=='c') then 
        varargout=list(n)
        else warning('option not implemented!')
        varargout=list([p,n])
    end
    if (rhs>1)&(lhs>1) then 
        error('only one output !')
    end
endfunction
