function [bool]=%s_o_mbigint(x,A)
    // overloading  compatibility for binary operator "==" ( equal)with double/mbigint
    // Calling Sequence
    //      x==A
    //   bool=(x==A)
    // Parameters
    //  A: mbigint
    //  x : double
    //  bool : boolean matrix
    //
    // Description
    // double/mbigint "equal" comparison
    //
    // Examples
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // z=bigint('1234567890') 
    // M=[x y; z x-z]
    // 123456789==M
    // 2345678==M
    // 1234567890==M
    // 123456789*ones(2,2)==M // matrix case
    // 
    // See also
    //  mbigint
    // bigint_o_mbigint
    // mbigint_o_mbigint
    //
    // Authors
    //  Philippe Roux
    //

    x=mbigint(x)
    bool=(x==A)
endfunction
