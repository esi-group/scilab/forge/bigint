function [str]=contribute(varargin)
    // bigint
    // Calling Sequence
    //   [str]=contribute([x[,y]])
    //
    // Parameters
    //  x,y: scilab variables
    //  str: string 
    //
    // Description
    //
    // bigint is a toolbox to make calculations with "big integers": their size (number of digits) can be arbitrary long  no rounding errros would appear. The toolbox consists only in macros (no java/c/... sources files) and should not necessit compilations.
    //<itemizedlist>
    // <listitem><para> a new variable type "bigint" build as a mlist </para></listitem>    
    // <listitem><para> another variable type "mbigint" for matrix of bigint  "bigint" build as a mlist </para></listitem> 
    // <listitem><para> arithmetic (+,-,*,^,/,...)  and boolean (==,...) operations are defined for bigint and mbigint type </para></listitem>    
    // <listitem><para> overloading compatibility for arithmetic (+,-,*,^,/,...)  and boolean (==,...) operations with double/bigint/mbigint operation </para></listitem>
    // <listitem><para> overloading basic scilab functions (as  size, sum, abs ...) isn't achieved </para></listitem>
    // </itemizedlist>    
    //  this function is just a "function skeleton" to contribute to bigint toolbox :
    // 
    //<itemizedlist>
    // <listitem><para>  open contribute.sci to see a "function skeleton" with online help written directly in the source code </para></listitem>
    // <listitem><para> add your own macros to bigint/macros/ directory  as *.sci files</para></listitem>    
    // <listitem><para> add your own examples in the  *.sci files, source code of demonstrations files will be generated in bigint/demos/ </para></listitem>
    // <listitem><para> add unitary tests in bigint/tests/unit_tests/ directory  as *.tst files  </para></listitem>
    // <listitem><para> run bigint/builder.sce to update the toolbox </para></listitem>
    // <listitem><para> at each scilab restart run bigint/loader.sce to to reload the toolbox (if macros runs  but online help isn't loaded  there is probably a mistake in the help xml code) </para></listitem>
    // </itemizedlist>
    // Online help is divided in chapters
     //<itemizedlist>
    // <listitem><para>  bigint : overloading of scilab binary/unary operators for bigint </para></listitem>
    // <listitem><para>  matrix_of_bigint : overloading of scilab binary/unary operators for mbigint </para></listitem>
    // <listitem><para>  extended_scilab_functions : extension of basic scilab function to bigint/mbigint (sum, size, abs ...) </para></listitem>
    // <listitem><para>  arithmetic : news scilab functions specific to bigint/mbigint </para></listitem>
    // </itemizedlist>
    //
    // Examples
    // contribute()  // basic usage
    // contribute('example') // how to use optional parameters
    // contribute(1,2) 
    // contribute('a',2,'c')
    // edit(fullfile(TMPDIR,'contribute')) //  see source code
    //
    // See also
    //  bigint
    //
    // Authors
    //  Philippe Roux
    //

    [lhs,rhs]=argn()
    if  rhs==0 then str='no optional arguments'
    else x=varargin(1)
        disp(x,'x=')
        str= 'one  optional argument '
        if rhs>1 then 
            y=varargin(2)
            disp(y,'y=')
            str=  'two  optional arguments '
            if rhs>2 then  str= 'many  optional arguments '
            end
        end
    end


    messagebox(['a '"function skeleton'"  to contribute to bigint toolbox.';' open contribute.sci to see how online help ';'is written directly in the source code.'])
endfunction
