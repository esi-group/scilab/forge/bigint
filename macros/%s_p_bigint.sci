function [z]=%s_p_bigint(x,y)
    // overloading  compatibility  for "^" (power)  operator with double/bigint
    // Calling Sequence
    //   z=x^y
    //
    // Parameters
    //  x : double(scalar or square matrix)
    //  y: bigint
    //  z :  mbigint
    //
    // Description
    // double/bigint power operator
    //
    // Examples
    //
    // format('v',20)
    // x=[1 2 ; 3 4] // 
    // y=bigint(10)
    // x^y
    // 
    // See also
    //  mbigint
    // mbigint_p_bigint
    // bigint_p_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    z=x^y
endfunction
