function M=%mbigint_c_mbigint(A,B)
    // line concatenation for  mbigint 
    // Calling Sequence
    //   M=[A B]
    //
    // Parameters
    //  M,A,B : mbigint
    //
    // Description
    // if A and B are bigint matrix of size pxn1 ans pxn2 then M is a bigint matrix of size px(n1+n2), otherwise produce error 5.
    //
    // Examples
    // //
    // x=bigint('123456789') 
    // y=bigint('02345678') 
    // A=[x;y]
    // B=[y;x]
    // M=[A B]
    // 
    // See also
    //  mbigint
    //
    // Authors
    //  Philippe Roux
    //
    
      M=mlist(['mbigint','display','value'],..
      [A.display B.display],..
      [A.value B.value])  
endfunction
