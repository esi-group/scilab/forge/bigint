function [bool]=%s_1_bigint(x,y)
    // overloading  compatibility for &#60; (lower) operator  with double/bigint
    // Calling Sequence
    //   x<y
    //   bool=(x<y)
    //
    // Parameters
    // y : bigint
    // x : double(scalar)
    // bool : boolean
    //
    // Description
    // double/bigint "lower" operator
    //
    // Examples
    //
    // x=12345678
    // y=bigint('123456789')
    // x<y  //  True
    // x=1234567890
    // x<y // false
    // (x*ones(2,2))<y  // matrix case
    // 
    // See also
    //  bigint
    // bigint_1_bigint
    // mbigint_1_bigint
    //
    // Authors
    //  Philippe Roux
    //
    
    // convert x to bigint
    x=mbigint(x)
    bool=(x<y)
endfunction
