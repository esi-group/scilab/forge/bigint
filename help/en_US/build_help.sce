function unixsed(fic)
    // unix('sed -i ''s/%\(.*\)_/\1_/g'' '+fic)
    txt = mgetl(fic);
    for i=grep(txt, '/%(.*)_/', 'r');
        // should be good to have : 
        // txt(i) = strsubst(txt(i) ,'/%(.*)_/' ,'\1','r');
        splited = strsplit(txt(i), ['%', '_']);
        splited(2:2:$) = splited(2:2:$) + '_';
        txt(i) = strcat(splited);
        mputl(txt,fic)
    end
endfunction

function sort_help_pages(help_lang_dir)
    //fichiers=listfiles(help_lang_dir+'*.xml')'
    fichiers=unix_g('ls -1 */*.xml')'
    for fic=fichiers
        fic=tokens(fic,'/');
        fic=fic($);
        if grep(fic, '/mbigint_(.*)_(.*)/', 'r')<>[] |grep(fic, '/(.*)_(.*)_mbigint/', 'r')<>[] then
            chap='matrix_of_bigint'
            printf('%s --> %s/\n',fic,chap)
        elseif grep(fic, '/bigint_(.*)_(.*)/', 'r')<>[] |grep(fic, '/(.*)_(.*)_bigint/', 'r')<>[] then
            chap='bigint'
            printf('%s --> %s/\n',fic,chap)
        elseif grep(fic, '/mbigint_[st0ep].xml/', 'r')<>[] then
            chap='matrix_of_bigint'
            printf('%s --> %s/\n',fic,chap)
            disp(ls(help_lang_dir+' *.xml'))
        elseif grep(fic, '/bigint_[st0ep].xml/', 'r')<>[] then
            chap='bigint'
            printf('%s --> %s/\n',fic,chap)
        elseif grep(fic, '/bigint_(.*)/', 'r')<>[] |grep(fic, '/mbigint_(.*)/', 'r')<>[] then
            chap='extended_scilab_functions'
            printf('%s --> %s/\n',fic,chap)
        elseif grep(fic, '/mbigint(.*)/', 'r')<>[] then
            chap='matrix_of_bigint'
            printf('%s --> %s/\n',fic,chap)
        elseif grep(fic, '/bigint(.*)/', 'r')<>[] then
            chap='bigint'
            printf('%s --> %s/\n',fic,chap)
        elseif grep(fic, '/(.*)help(.*)/', 'r')==[]|grep(fic, '/(.*)contribute(.*)/', 'r')==[] then
            chap='arithmetic'
            printf('%s --> %s/\n',fic,chap)
        else chap='.'
            printf('%s --> %s/\n',fic,chap)
        end
        movefile(help_lang_dir+fic,help_lang_dir+chap+'/'+fic)
    end
endfunction

help_lang_dir = get_absolute_file_path('build_help.sce');
help_from_sci('../macros/','../help/en_US','../demos/'); // creating demodir/*.dem.sce demo files.
//  remove % from xml id in help files 
printf('remove *percent* from xml id in help files \n')
for fic=listfiles(help_lang_dir+'*.xml')'
    //unixsed(fic) //
    unix('sed -i ''s/%\(.*\)_/\1_/g'' '+fic)
    //printf('%s\n',fic)
end
//  remove % from macros corresponding to overloaded operators
printf('remove *percent* from macros names corresponding to overloaded operators\n')
for fic=listfiles(help_lang_dir+'%*_*.xml')'
    newfic=strsubst(fic,'%','')
    //printf('%s\n',newfic)    
    movefile(fic,newfic)
end
// help chapters
printf('move help files to chapters\n')
sort_help_pages(help_lang_dir)
//for fic=listfiles(help_lang_dir+'*.xml')'
//    fic=tokens(fic,'/')
//    fic=fic($)
//    if grep(fic,'mbigint')<>[] then
//       chap='matrix_bigint/'
//    elseif grep(fic,'bigint')<>[] then
//        chap='bigint/'
//    else chap='arithmetic/'
//    end
//    movefile(help_lang_dir+fic,help_lang_dir+chap+fic)
//    //printf(fic+'->'+chap+fic+'\n')
//end

tbx_build_help(toolbox_title, help_lang_dir);
clear help_lang_dir;
