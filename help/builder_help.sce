help_dir = get_absolute_file_path('builder_help.sce');
mdelete(help_dir+'/../demos/*.sce')
mdelete(help_dir+'/../jar/*')
mdelete(help_dir+'/en_US/*.xml')
mdelete(help_dir+'/en_US/*/*')
help_dir_US = pathconvert( help_dir ) + "en_US" + filesep();
tbx_builder_help_lang("en_US", help_dir);
tbx_build_help_loader(toolbox_title, help_dir)
