mode(1)
//
// Demo of %s_q_mbigint.sci
//

//
x=123456789
xx=bigint(x)
y=9876543
yy=bigint(y)
x*y
M=[xx yy; -xx -xx]
N=[y x; y -y]
N.\M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
