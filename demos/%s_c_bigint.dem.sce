mode(1)
//
// Demo of %s_c_bigint.sci
//

//
y=bigint('123456789')
x=1
M=[x y]
y=[1:10]   // matrix case
L=[x y]
y=[]        // empty case
L=[x y]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
