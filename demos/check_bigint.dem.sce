mode(1)
//
// Demo of check_bigint.sci
//

halt()   // Press return to continue
 
x=bigint('-1234567890')
x=check_bigint(x) // OK
x.signe=%pi
x=check_bigint(x) // error
x=bigint('0')
x.signe=-1
x=check_bigint(x) // change signe to +
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
