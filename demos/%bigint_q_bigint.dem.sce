mode(1)
//
// Demo of %bigint_q_bigint.sci
//

halt()   // Press return to continue
 
y=bigint('98765432')
x=bigint('12345678')
q=x\y  //  bigint division
98765432/12345678
r=x-q*y
pmodulo(98765432,12345678) //  division with doubles
x=bigint('1234567890')
y=bigint('987654321123456789')
q=y.\x
r=x-q*y
x== q*y+r
r<abs(x)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
