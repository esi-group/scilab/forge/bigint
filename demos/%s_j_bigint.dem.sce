mode(1)
//
// Demo of %s_j_bigint.sci
//

//
x=bigint('45')
M=[0 1 ; 2 3]
M.^x
2^x
3^x
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
