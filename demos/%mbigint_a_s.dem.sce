mode(1)
//
// Demo of %mbigint_a_s.sci
//

//
x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[x y; z x-z]
M+123456789             // scalar case
M+(123456789*ones(2,2)) // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
