mode(1)
//
// Demo of %bigint_s.sci
//

x=bigint('-1234567890')
-x
// special case "-0=0"
x=bigint('0')
-x
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
