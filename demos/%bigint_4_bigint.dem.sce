mode(1)
//
// Demo of %bigint_4_bigint.sci
//

//
x=bigint('123456789')
y=bigint('0123456789')
z=bigint('1234567890')
x>=y  // true
x>=z  // false
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
