mode(1)
//
// Demo of %mbigint_e.sci
//

halt()   // Press return to continue
 
x=bigint('123456789')
y=bigint('02345678')
M=[x y;x-y bigint(0)]
M(2,2)
M(1,:) // first line
M(:,2) // second column
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
