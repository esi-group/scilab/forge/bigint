mode(1)
//
// Demo of %mbigint_s_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[y y; bigint(0) x]
N=[x y; z x-y]
M-N
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
