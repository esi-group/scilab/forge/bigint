mode(1)
//
// Demo of %mbigint_x_s.sci
//

//
x=123456
y=456789
z=987654
MM=[x y; z x-z]
M=mbigint(MM)
N=[x -z; -y x-z]
M*N
MM*N //double verification
P=[x x;x x]
N*P
N*bigint(x)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
