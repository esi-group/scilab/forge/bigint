mode(1)
//
// Demo of millerrabin.sci
//

//
x=bigint(2)^17-1, //Mersenne  prime
millerrabin(x)
y=bigint(2)^23-1, //Mersenne not prime
millerrabin(y)
factorize(y)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
