mode(1)
//
// Demo of %bigint_n_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
A=[x y]
B=[y x]
C=[x x]
x<>A
x<>B
x<>C
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
