mode(1)
//
// Demo of %mbigint_1_s.sci
//

x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[x y; z x-z]
M<123456789
M<2345678
M<-123456789
M<[987654321 123; 0 -123]
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
