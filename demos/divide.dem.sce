mode(1)
//
// Demo of divide.sci
//

halt()   // Press return to continue
 
x=bigint('98765432')
y=bigint('12345678')
[q,r]=divide(x,y)  //  bigint division
98765432/12345678  // totient
pmodulo(98765432,12345678) // reminder
y=bigint('1234567890')
x=bigint('987654321123456789')
[q,r]=divide(x,y)
x== q*y+r
r<abs(y)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
