mode(1)
//
// Demo of %s_3_mbigint.sci
//

x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[x y; z x-z]
123456789<=M
2345678<=M
-123456789<=M
//matrix case
[123456789 2345678; -123456789 0]<=M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
