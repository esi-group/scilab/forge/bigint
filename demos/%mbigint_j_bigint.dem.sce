mode(1)
//
// Demo of %mbigint_j_bigint.sci
//

//
x=bigint('12345')
y=bigint('98765')
M=[x y; y x]
N=bigint(3)
M.^N
y^3
x^3
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
