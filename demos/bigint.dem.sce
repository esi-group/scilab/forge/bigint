mode(1)
//
// Demo of bigint.sci
//

//a 10 digit integer
bigint 1234567890987654321 // big int stored in ans
x=ans
x=bigint('-1234567890987654321') // use quote syntax for strings
x=bigint('00000000000000000') //=0
x=bigint('-0') // =0
x=bigint(123456789)  // double<10^16 or uint* to bigint convertion
bigint(-123456789987654321) // probable rounding error
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
