mode(1)
//
// Demo of %mbigint_x_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('9876543')
x*y
M=[x y; -x -x]
N=[y x; y -y]
M.*N
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
