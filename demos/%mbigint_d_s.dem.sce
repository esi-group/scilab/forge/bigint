mode(1)
//
// Demo of %mbigint_d_s.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
z=123456789
M=[y x; -x -y]
N=[x x;x x]
M./z
N./z
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
