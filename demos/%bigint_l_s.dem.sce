mode(1)
//
// Demo of %bigint_l_s.sci
//

halt()   // Press return to continue
 
y=98765432
x=bigint('12345678')
q=x\y  //  bigint division
98765432/12345678
r=y-q*x
pmodulo(98765432,12345678) //  division with doubles
y== q*x+r
r<abs(x)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
