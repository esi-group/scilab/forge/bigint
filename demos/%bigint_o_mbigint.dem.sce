mode(1)
//
// Demo of %bigint_o_mbigint.sci
//

x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[x y; z x-z]
x==M
y==M
z==M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
