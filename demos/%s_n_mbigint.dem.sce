mode(1)
//
// Demo of %s_n_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('987654321')
A=[x y]
B=[y x]
C=[x x]
123456789<>A
123456789<>B
123456789<>C
[123456789, 987654321]<>A // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
