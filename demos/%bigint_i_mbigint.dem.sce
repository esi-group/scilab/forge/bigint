mode(1)
//
// Demo of %bigint_i_mbigint.sci
//

halt()   // Press return to continue
 
x=bigint('123456789')
y=bigint('02345678')
M=[x y;x-y bigint(0)]
M(2,2)=y-x
M(1,1:2)=-x
M(1:2,2)=-y
M(1:3)= x   // index instead of line/column
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
