mode(1)
//
// Demo of Bezout.sci
//

halt()   // Press return to continue
 
x=bigint('98765432')
y=bigint('12345678')
[d,a,b]=Bezout(x,y)
a*x+b*y==d
x=bigint('123456789987654321')
y=bigint('987654321123456789')
[d,a,b]=Bezout(x,y)
a*x+b*y==d
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
