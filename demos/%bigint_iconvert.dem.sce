mode(1)
//
// Demo of %bigint_iconvert.sci
//

//
x=brand(1e10)
typeof(x)
y=double(x)
typeof(y)
x=brand(1e20) // rounding error
y=double(x)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
