mode(1)
//
// Demo of %s_l_mbigint.sci
//

//
x=1234567890
y=bigint('987654321')
M=[y x; -x -y]
N=[x x;x x]
x\M    // scalar case
N\M    // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
