mode(1)
//
// Demo of %s_a_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[x y; z x-z]
123456789+M             // scalar case
(123456789*ones(2,2))+M // matrix case
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
