mode(1)
//
// Demo of %s_j_mbigint.sci
//

//
x=12345
M=mbigint([0 1; 2 3])
x.^N
x^2
x^3
M.^mbigint(M)
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
