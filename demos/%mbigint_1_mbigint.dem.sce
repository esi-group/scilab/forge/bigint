mode(1)
//
// Demo of %mbigint_1_mbigint.sci
//

//
x=bigint('123456789')
y=bigint('02345678')
z=bigint('1234567890')
M=[x x; x x]
N=[x y; z x-z]
M<N
N<M
M<M
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
